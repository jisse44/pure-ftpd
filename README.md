# pure-ftpd
pure-ftpd 1.0.43 for Debian 9 with Mariadb 10.3 compatibility

-----------------------------------------
This is a patched version of pure-ftpd-mysql with Mariadb 10.3 compatibility, to avoid libmariadbclient.so.18 dependency error:

pure-ftpd-mysql[9630]: /usr/sbin/pure-ftpd-mysql-virtualchroot: relocation error: /usr/sbin/pure-ftpd-mysql-virtualchroot: symbol my_make_scrambled_password, version libmariadbclient_18 not defined in file libmariadbclient.so.18 with link time reference


Patch applied is descibed here: https://github.com/jedisct1/pure-ftpd/commit/27443b29320d85352d8b52c0120836843e10c0f9

Bug is decribed here: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=889346


To build yourself for another Mariadb version, read HowTo.txt
